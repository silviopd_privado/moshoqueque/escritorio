/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import accesodatos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import util.Funciones;

/**
 *
 * @author silviopd
 */
public class InicioSesion extends Conexion{
    
    private String dni,password;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public int iniciarSesion() throws Exception {

        String sql = "select * from f_iniciar_sesion(?, ?)";

        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setString(1, this.getDni());
        sentencia.setString(2, this.getPassword());

        ResultSet resultado = this.ejecutarSQL(sentencia);

        if (resultado.next()){
            if (resultado.getInt("estado") == 2 ){
               
                Funciones.NOMBRE_LOGUEADO =resultado.getString("nombre_completo");
                Funciones.DNI_LOGUEADO =resultado.getString("dni");
                                
                return 2; 
            }
        }
        return 1;
    }
    
    public ResultSet acceso() throws Exception {

        String sql = "select * from f_acceso(?)";

        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setString(1, this.getDni());

        ResultSet resultado = this.ejecutarSQL(sentencia);
        
        return resultado;
    }
}
