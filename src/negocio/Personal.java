/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import accesodatos.Conexion;

/**
 *
 * @author silviopd
 */
public class Personal extends Conexion {

    private String dni, apellido_paterno, apellido_materno, nombres, direccion, telefono_fijo, telefono_movil1, telefono_movil2, email, dni_jefe;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono_fijo() {
        return telefono_fijo;
    }

    public void setTelefono_fijo(String telefono_fijo) {
        this.telefono_fijo = telefono_fijo;
    }

    public String getTelefono_movil1() {
        return telefono_movil1;
    }

    public void setTelefono_movil1(String telefono_movil1) {
        this.telefono_movil1 = telefono_movil1;
    }

    public String getTelefono_movil2() {
        return telefono_movil2;
    }

    public void setTelefono_movil2(String telefono_movil2) {
        this.telefono_movil2 = telefono_movil2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDni_jefe() {
        return dni_jefe;
    }

    public void setDni_jefe(String dni_jefe) {
        this.dni_jefe = dni_jefe;
    }

}
