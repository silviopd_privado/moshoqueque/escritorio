package negocio;

import accesodatos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Usuario extends Conexion {

    private String dni, pass, estado;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String[] obtenerCamposBusqueda() {
        String campos[] = {"dni","paterno","materno","nombres"};
        return campos;
    }
    
    public ResultSet listar() throws Exception {
        String sql = "select * from v_usuario_listar;";
        PreparedStatement sentencia = abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

        ResultSet resultado = ejecutarSQL(sentencia);
        return resultado;
    }

}
