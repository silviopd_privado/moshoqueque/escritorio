/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import java.awt.Toolkit;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import negocio.InicioSesion;
import util.Funciones;
import util.ImagenFondo;

/**
 *
 * @author silviopd
 */
public class frmPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form frmPrinciapl
     */
    public frmPrincipal() {
        initComponents();

        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setTitle(Funciones.NOMBRE_SOFTWARE + "Menu Principal");

        new Funciones().cambiarIconoFormulario(this, "/imagenes/icon.png");
        this.lblFecha.setText("Fecha: " + Funciones.obtenerFechaActual());

        this.contenedor.setBorder(new ImagenFondo("extendido"));

        ocultarMenu();
    }

    private void ocultarMenu() {
        this.mantenimiento.setVisible(false);
        this.administracion.setVisible(false);
    }

    private void mostrarMenu() {
        try {
            InicioSesion obj = new InicioSesion();
            obj.setDni(Funciones.DNI_LOGUEADO);

            ResultSet resultado;

            resultado = obj.acceso();

            String menu;
            while (resultado.next()) {
                menu = resultado.getString("nombre");

                switch (menu) {
                    case "mantenimiento":
                        this.mantenimiento.setVisible(true);
                        break;
                    case "administracion":
                        this.administracion.setVisible(true);
                        break;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(frmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void obtenerDimensionesContenedor() {
        Funciones.ALTO_CONTENEDOR = this.contenedor.getHeight();
        Funciones.ANCHO_CONTENEDOR = this.contenedor.getWidth();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem9 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        contenedor = new javax.swing.JDesktopPane();
        jToolBar1 = new javax.swing.JToolBar();
        jSeparator15 = new javax.swing.JToolBar.Separator();
        jLabel1 = new javax.swing.JLabel();
        jSeparator13 = new javax.swing.JToolBar.Separator();
        lblUsuario = new javax.swing.JLabel();
        jSeparator14 = new javax.swing.JToolBar.Separator();
        lblFecha = new javax.swing.JLabel();
        menus = new javax.swing.JMenuBar();
        usuario = new javax.swing.JMenu();
        btnCambiarContraseña = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        btnCerrarSesion = new javax.swing.JMenuItem();
        mantenimiento = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        reporte = new javax.swing.JMenu();
        busqueda = new javax.swing.JMenu();
        administracion = new javax.swing.JMenu();
        btnCrearUsuario = new javax.swing.JMenuItem();
        btnPermisos = new javax.swing.JMenuItem();
        ayuda = new javax.swing.JMenu();
        btnManualDeUsuario = new javax.swing.JMenuItem();
        btnAcercaDe = new javax.swing.JMenuItem();

        jMenuItem9.setText("jMenuItem9");

        jMenu2.setText("jMenu2");

        jMenu1.setText("jMenu1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        javax.swing.GroupLayout contenedorLayout = new javax.swing.GroupLayout(contenedor);
        contenedor.setLayout(contenedorLayout);
        contenedorLayout.setHorizontalGroup(
            contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 776, Short.MAX_VALUE)
        );
        contenedorLayout.setVerticalGroup(
            contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 274, Short.MAX_VALUE)
        );

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setEnabled(false);
        jToolBar1.add(jSeparator15);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bienvenido.png"))); // NOI18N
        jLabel1.setText("Bienvenido");
        jToolBar1.add(jLabel1);
        jToolBar1.add(jSeparator13);

        lblUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/usuario.png"))); // NOI18N
        lblUsuario.setText("Usuario:");
        jToolBar1.add(lblUsuario);
        jToolBar1.add(jSeparator14);

        lblFecha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/calendario.png"))); // NOI18N
        lblFecha.setText("jLabel3");
        jToolBar1.add(lblFecha);

        usuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/usuario.png"))); // NOI18N
        usuario.setText("Usuario");

        btnCambiarContraseña.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/key.png"))); // NOI18N
        btnCambiarContraseña.setText("Cambiar Contraseña");
        usuario.add(btnCambiarContraseña);
        usuario.add(jSeparator1);

        btnCerrarSesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/lock.png"))); // NOI18N
        btnCerrarSesion.setText("Cerrar Sesión");
        btnCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarSesionActionPerformed(evt);
            }
        });
        usuario.add(btnCerrarSesion);

        menus.add(usuario);

        mantenimiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/maintenance.png"))); // NOI18N
        mantenimiento.setText("Mantenimiento");

        jMenu4.setText("Personal");

        jMenuItem2.setText("Listar");
        jMenu4.add(jMenuItem2);

        mantenimiento.add(jMenu4);

        jMenu3.setText("Ubigeo");

        jMenuItem1.setText("Listar Departamento");
        jMenu3.add(jMenuItem1);

        jMenuItem3.setText("Listar Provincia");
        jMenu3.add(jMenuItem3);

        jMenuItem4.setText("Listar Distrito");
        jMenu3.add(jMenuItem4);

        mantenimiento.add(jMenu3);

        menus.add(mantenimiento);

        reporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/report.png"))); // NOI18N
        reporte.setText("Reporte");
        menus.add(reporte);

        busqueda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/search.png"))); // NOI18N
        busqueda.setText("Busqueda");
        menus.add(busqueda);

        administracion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/admin.png"))); // NOI18N
        administracion.setText("Administración del sistema");

        btnCrearUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/usuario.png"))); // NOI18N
        btnCrearUsuario.setText("Usuarios");
        btnCrearUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearUsuarioActionPerformed(evt);
            }
        });
        administracion.add(btnCrearUsuario);

        btnPermisos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/access.png"))); // NOI18N
        btnPermisos.setText("Permisos");
        administracion.add(btnPermisos);

        menus.add(administracion);

        ayuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/help.png"))); // NOI18N
        ayuda.setText("Ayuda");

        btnManualDeUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/document_user.png"))); // NOI18N
        btnManualDeUsuario.setText("Manual de Usuario");
        ayuda.add(btnManualDeUsuario);

        btnAcercaDe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/about.png"))); // NOI18N
        btnAcercaDe.setText("Acerca de ");
        ayuda.add(btnAcercaDe);

        menus.add(ayuda);

        setJMenuBar(menus);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contenedor)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(contenedor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        frmInicioSesion objFormInicio = new frmInicioSesion(this, true);
        objFormInicio.setLocationRelativeTo(null);
        objFormInicio.setVisible(true);
        if (objFormInicio.respuesta == 1) {
            dispose();
        } else {
            mostrarMenu();
        }
    }//GEN-LAST:event_formWindowOpened

    private void btnCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarSesionActionPerformed
        Funciones.NOMBRE_LOGUEADO = "";
        Funciones.DNI_LOGUEADO = "";

        ocultarMenu();

        frmInicioSesion objFormInicio = new frmInicioSesion(this, true);
        objFormInicio.setLocationRelativeTo(null);
        objFormInicio.setVisible(true);
        if (objFormInicio.respuesta == 1) {
            dispose();
        }
    }//GEN-LAST:event_btnCerrarSesionActionPerformed

    private void btnCrearUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearUsuarioActionPerformed
        frmUsuarioListar objFrmClienteListado= new frmUsuarioListar(this.contenedor.getWidth()/2,this.contenedor.getHeight()/2,this.contenedor.getWidth(),this.contenedor.getHeight());
        this.contenedor.add(objFrmClienteListado);
        objFrmClienteListado.setVisible(true);
    }//GEN-LAST:event_btnCrearUsuarioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu administracion;
    private javax.swing.JMenu ayuda;
    private javax.swing.JMenuItem btnAcercaDe;
    private javax.swing.JMenuItem btnCambiarContraseña;
    private javax.swing.JMenuItem btnCerrarSesion;
    private javax.swing.JMenuItem btnCrearUsuario;
    private javax.swing.JMenuItem btnManualDeUsuario;
    private javax.swing.JMenuItem btnPermisos;
    private javax.swing.JMenu busqueda;
    private javax.swing.JDesktopPane contenedor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator13;
    private javax.swing.JToolBar.Separator jSeparator14;
    private javax.swing.JToolBar.Separator jSeparator15;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblFecha;
    public static javax.swing.JLabel lblUsuario;
    private javax.swing.JMenu mantenimiento;
    private javax.swing.JMenuBar menus;
    public static javax.swing.JMenu reporte;
    private javax.swing.JMenu usuario;
    // End of variables declaration//GEN-END:variables
}
